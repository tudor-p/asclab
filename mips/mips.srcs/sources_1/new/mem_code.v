`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: TUIASI
// Engineer: Tudor P
// 
// Create Date: 04/04/2018 12:00:07 PM
// Design Name: 
// Module Name: mem_code
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mem_code(
    input [(ADDR_WIDTH-1):0] addr,
    output [(INSTR_WIDTH-1):0] instr
    );
    parameter INSTR_WIDTH=32;
    parameter ADDR_WIDTH=8;
    reg [(INSTR_WIDTH-1):0] mem [0:((1<<ADDR_WIDTH)-1)];
    assign instr=mem[addr];
endmodule
