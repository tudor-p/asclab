`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: TUIASI
// Engineer: Tudor P
// 
// Create Date: 04/04/2018 03:57:42 PM
// Design Name: 
// Module Name: tb_mem_code_simple
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module tb_mem_code_simple();
parameter INSTR_WIDTH=32;
    parameter ADDR_WIDTH=8;
    reg [(ADDR_WIDTH-1):0] addr;
    wire [(INSTR_WIDTH-1):0] instr;
    mem_code#(.ADDR_WIDTH(ADDR_WIDTH), .INSTR_WIDTH(INSTR_WIDTH)) code(.addr(addr), .instr(instr));
    initial
    begin
        $readmemh("tb_mem_code.mem", code.mem, 0, 4);
        //check values at addresses 0, 1, 2, 3 to match values in memory file tb_mem_code_simple
        addr=0; #1
        if (3 != instr)
            begin
                $display("!!!UNEXPECTED INSTRUCTION FOUND IN MEMORY AT ADDRESS %d", addr);
                $finish;
            end
        addr=1; #1
        if (1 != instr)
            begin
                $display("!!!UNEXPECTED INSTRUCTION FOUND IN MEMORY AT ADDRESS %d", addr);
                $finish;
            end
        addr=2; #1
        if (2 != instr)
            begin
                $display("!!!UNEXPECTED INSTRUCTION FOUND IN MEMORY AT ADDRESS %d", addr);
                $finish;
            end
        addr=3; #1
        if (3 != instr)
            begin
                $display("!!!UNEXPECTED INSTRUCTION FOUND IN MEMORY AT ADDRESS %d", addr);
                $finish;
            end
        $display("SIMPLE CODE MEMORY TEST OK!");
        #1 $finish;
    end
endmodule
