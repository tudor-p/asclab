`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: TUIASI
// Engineer: Tudor P
// 
// Create Date: 04/04/2018 12:02:28 PM
// Design Name: 
// Module Name: tb_mem_code
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_mem_code();
    parameter INSTR_WIDTH=32;
    parameter ADDR_WIDTH=8;
    reg [(ADDR_WIDTH-1):0] addr;
    reg [(ADDR_WIDTH-1):0] lim;
    wire [(INSTR_WIDTH-1):0] instr;
    mem_code#(.ADDR_WIDTH(ADDR_WIDTH), .INSTR_WIDTH(INSTR_WIDTH)) code(.addr(addr), .instr(instr));
    initial
    begin
        $readmemh("tb_mem_code.mem", code.mem ,0,4);
        #1 addr=0; #1
        if (instr > 0 && instr < (1<<ADDR_WIDTH))
            begin
                lim = instr;
                for(addr = 1;addr<=lim;addr=addr + 1)
                begin
                    #1                
                    if(instr != addr)
                    begin
                        $display("!!!UNEXPECTED INSTRUCTION FOUND IN MEMORY AT ADDRESS %d; expected %d, but found %d;", addr, addr, instr);
                        $finish;
                    end
                end
            end
        else
            begin
                $display("!!!UNEXPECTED INSTRUCTION TEST LENGTH!");
                $finish;
            end
        $display("CODE MEMORY TEST OK!");
        #1 $finish;
    end
endmodule
