//©2009 Popovici Alexandru-Tudor 2009-05-07
#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

unsigned long comps1 = 0, comps2 = 0;
int compare (const void * a, const void * b)
{
	++comps1;
  return ( *(int*)a - *(int*)b );
}
void shuffle(int *v, int n)
{
	for(int i = 0;i<n;++i)
	{
		swap(v[i], v[rand()%n]);
	}
}

void qsortASM(int *v, int n)
{
	shuffle(v, n);
	int *end = v+n-1;
	__asm
	{  
		;1e6
		;5.663
		;1.5e6
		;10.998
		push end
		push v
		mov ecx, 1
	qsortIn:
		test ecx, ecx
		jz qsortOut
		mov esi, [esp+4]
		mov edi, [esp]
		cmp	esi, edi
		jg qpart
		add	esp, 8				;throw Stack						;;;;
		dec ecx
		jmp qsortIn
	qpart:
		mov edx, [edi]				; x = a[l]
	while0:
		cmp edi, esi
		jge out0
	while1:
		cmp [edi], edx				;a[l]<=x
		jg while2
		cmp edi, [esp+4]			;l<=high
		jg while2
		add edi, 4					;sizeof!!!
		jmp while1

		mov eax, [esp]
	while2:
		cmp [esi], edx				;a[h]>x
		jle out1
		cmp esi, eax				;h>=low
		jl out1
		sub esi, 4					;sizeof!!!
		jmp while2
	out1:
		cmp edi, esi				;l<h
		jge skipInterIn
		mov eax, [esi]											;;;;
		xchg eax, [edi]				;xchg a[l] a[h]				;;;;
		xchg eax, [esi]											;;;;
	skipInterIn:
		jmp while0
	out0:
		mov eax, [esi]											;;;;
		mov ebx, [esp]			
		xchg eax, [ebx]											;;;;
		xchg eax, [esi]											;;;;

		add esi, 4
		mov [esp], esi				;push avoid
		sub	esi, 8					;sizeof!!!
		push esi
		push ebx
		inc ecx
		jmp qsortIn
	qsortOut:
	}
}
void shell(int *v, int n)
{
	int *end = v+n;
	long insp=0;
	int d=1;
	__asm
	{  
			mov     eax, n
			mov     edx, 1
			shr     eax, 1
		buildh:
			shl     edx, 1
			shr     eax, 1
			cmp     eax, 0
			jg buildh 
			;mov     edx, 1
			;shl     edx, 6     ;init h, dx=h
		foro:
			dec     edx         ;dx = k
			shl     edx, 2			;!!!!!!!!!!!depends of sizeof(tip)
			mov     ebx, v   ;i
			add     ebx, edx     ;pointer la temp
		foris:
			cmp     ebx, end
			jnl     skipforis
			mov     ecx, [ebx]		;cx = temp
			mov		esi, ebx		;salvez i
			sub     ebx, edx		;limit = j  ;bx = j
		while1:
			cmp     ebx, v			;j>=0
			jl      skipw
			cmp     ecx, [ebx]		;temp<vect[j]
			jge     skipw
			mov     eax, [ebx]		;ax = a[j]
			add     ebx, edx
			mov     [ebx], eax		;a[j+k] = a[j]
			sub     ebx, edx		;limit = j
			sub     ebx, edx		;j = j-k;
			jmp while1
		skipw:        
			add     ebx, edx
			cmp     ebx, esi
			je      skipif
			mov     [ebx], ecx
		skipif:
			mov		ebx, esi
			add     ebx, 4			;!!!!!!!!!!!!!! sizeof(tip)
			jmp      foris
		skipforis:
			shr     edx, 2			;!!!!!!!!!!!!!!!sizeof(tip)dependent
			inc     edx				;dx=h
			shr     edx, 1			;half h
			cmp     edx, 1
			jg      foro       
	}
}
int qPartCpp(int *v, size_t low, size_t high)
{
	int l = low;
	int h = high;
	int x = v[low];
	while(l<h)
	{
		while((v[l]<=x)&&(l<=high))
		{
			++comps2;
			++l;
		}
		++comps2;
		while((v[h]>x)&&(h>=low))
		{
			++comps2;
			--h;
		}
		++comps2;
		if(l<h)
		{
			swap(v[l],v[h]);
		}
	}
	swap(v[h],v[low]);
	return h;
}
void qSortCpp(int *v, int low, int high)
{
	if(low<high)
	{
		int k=qPartCpp(v, low, high);
		qSortCpp(v, low, k-1);
		qSortCpp(v, k+1, high);
	}
}
void qSortCppOpti(int *v, int low, int high)
{
	while(low<high)
	{
		int k=qPartCpp(v, low, high);
		if(k-low>high-k)
		{
			qSortCpp(v, k+1, high);
			high = k-1;
		}
		else
		{
			qSortCpp(v, low, k-1);
			low = k+1;
		}
		
	}
}
void Merge(int *s, int b, int m, int e)
{
	int l = e - b + 1;
	int *d = new int[l];
	int i = b;
	int j = m+1;
	int k = -1;
	while(i<=m && j<=e)
	{
		if(s[i]<s[j])
		{
			d[++k] = s[i++];
		}
		else
		{
			d[++k] =s[j++];
		}
	}
	if(i<=m)
	{
		d[++k] = s[i];
		while(i<m)
		{
			d[++k] = s[++i];
		}
	}
	if(j<=e)
	{
		d[++k] = s[j];
		while(j<e)
		{
			d[++k] = s[++j];
		}
	}
	k = 0;
	for(int i = b; k<l;++i, ++k)
	{
		s[i] = d[k];
	}
	delete[] d;
}
void MergeSort(int *s, int b, int e)
{
	if(b<e)
	{
		int m = (b+e)/2;
		MergeSort(s, b, m);
		MergeSort(s, m+1, e);
		Merge(s, b, m, e);
	}
}
template <class T>
void shellSort(T *a,size_t n)
{
	int i,j;
	unsigned long h,k;
	T temp;
	int t = n;
	h=1;
	while(t>0)
	{
		h<<=1;
		t>>=1;
	}
	for(;h>0;h>>=1)
	{
		k=h-1;
		for(i=k;i<n;++i)
		{
			temp=a[i];
			j=i-k;
			while((j>=0) && (temp < a[j]))
			{
				a[j+k]=a[j];
				j-=k;
			}
			if(j+k !=i)
			{
				a[j+k]=temp;
			}
		}
	}
}
void shellSortS(int *a,size_t n)
{
	int i,j;
	unsigned long h,k;
	int temp;
	
	int t = n;
	h=1;
	while(t>0)
	{
		h<<=1;
		t>>=1;
	}
	for(;h>0;h>>=1)
	{
		k=h-1;
		for(i=k;i<n;++i)
		{
			temp=a[i];
			j=i-k;
			while((j>=0) && (temp < a[j]))
			{
				a[j+k]=a[j];
				j-=k;
			}
			if(j+k !=i)
			{
				a[j+k]=temp;
			}
		}
	}
}
//vreau o functie care sa interschimbe valorile din a si b intre ele
//int * const a <=> &a
void swap_meu(int const  * const a, int * const b)
{
	int * z = (int*)a;
	*z = 0;
	int t = *a;
	//*a = *b;
	*b = t;
}

void swap_ref(int const &a, int &b)
{
	int *z = (int*)a;
	int t = a;
	//a = b;
	b = t;
}
void swap_rau(int a, int b)
{
	int t = a;
	a = b;
	b = t;
}

int main()
{
	int x = 1, y = 2;
	cout << x << " " << y << endl;
	swap_meu(&x, &y);
	swap_ref(x, y);
	swap_rau(x, y);
	cout << x << " " << y << endl;
	return 0;
	//concluzii
	//pt random      t(shellASM) < t(qsort), n<~2.5e6 | 5e7 -> asm 240s, qs 45s!!!
	//pt sortat inv  shellASM rulllz (2s pt n=1e7) (11.5s n=5e7 qs 53s!!!)

	//qsortASM, mai bun pana la 19.3e6
	//5e7 sorted results:
	//ShellSort ASM:  10.326
	//QSort ASM:     127.515
	//QSortCppO:     297.889 comps: 104994217
	//MergeSort:	  74.78
	//QSort cstdlib:  53.424 comps: 1240948122
	//QSort Cpp:     294.048 comps: 3056844931Pre
	int c = 50000000;
	int *v = new int[c];
	int *w = new int[c];
	srand(time(0));
	for(int i = 0;i<c;++i)
	{
		//v[i] = rand();
		//v[i] = c-i;
		//v[i]= w[i] = c-i;
		//v[i] = w[i] = i;
		v[i] = w[i] = rand();
	}
	
	clock_t st = clock();
	shell(v, c);
	clock_t end = clock();
	cout<<"\nShellSort ASM: "<<((double)(end-st))/CLOCKS_PER_SEC;
	for(int i = 1;i<c;++i)
	{
		if(v[i-1] > v[i])
			cout<<"error";
	}
	for(int i = 0;i<c;++i)
	{
		//v[i]=c-i;
		v[i] = w[i];
	}
	
	/*st = clock();
	shellSort(v, c);
	end = clock();
	cout<<"\nShellSort <T>: "<<((double)(end-st))/CLOCKS_PER_SEC;
	for(int i = 0;i<c;++i)
	{
		//v[i]=c-i;
		v[i] = w[i];
	}
	st = clock();
	shellSortS(v, c);
	end = clock();
	cout<<"\nShellSort Cpp: "<<((double)(end-st))/CLOCKS_PER_SEC;
	for(int i = 0;i<c;++i)
	{
		//v[i]=rand();
		v[i] = w[i];
	}*/
	st = clock();
	qsortASM(v, c);
	end = clock();
	cout<<"\nQSort ASM: "<<((double)(end-st))/CLOCKS_PER_SEC;
	for(int i = 1;i<c;++i) if(v[i-1]>v[i]) cout<<"Err";
	for(int i = 0;i<c;++i)
	{
		//v[i]=rand();
		//v[i] = c-i;
		v[i] = w[i];
	}
	comps2 = 0;
	st = clock();
	shuffle(v, c);
	qSortCppOpti(v, 0, c-1);
	end = clock();
	cout<<"\nQSortCppO: "<<((double)(end-st))/CLOCKS_PER_SEC<<" comps: "<<comps2;
	for(int i = 1;i<c;++i) if(v[i-1]>v[i]) cout<<"Err";
	for(int i = 0;i<c;++i)
	{
		//v[i]=rand();
		//v[i] = c-i;
		v[i] = w[i];
	}
	st = clock();
	MergeSort(v, 0 , c-1);
	end = clock();
	cout<<"\nMergeSort: "<<((double)(end-st))/CLOCKS_PER_SEC;
	for(int i = 1;i<c;++i) if(v[i-1]>v[i]) cout<<"Err";
	for(int i = 0;i<c;++i)
	{
		//v[i]=rand();
		//v[i] = c-i;
		v[i] = w[i];
	}
	st = clock();
	qsort (v, c, sizeof(int), compare);
	end = clock();
	cout<<"\nQSort cstdlib: "<<((double)(end-st))/CLOCKS_PER_SEC<<" comps: "<<comps1;	
	for(int i = 1;i<c;++i) if(v[i-1]>v[i]) cout<<"Err";
	for(int i = 0;i<c;++i)
	{
		//v[i]=rand();
		//v[i] = c-i;
		v[i] = w[i];
	}
	comps2=0;
	st = clock();
	shuffle(v, c);
	qSortCpp(v, 0, c-1);
	end = clock();
	cout<<"\nQSort Cpp: "<<((double)(end-st))/CLOCKS_PER_SEC<<" comps: "<<comps2;
	delete[] v;
	delete[] w;
	cin >> c;
}
